#!/bin/sh sudo docker images -qf dangling=true | xargs sudo docker rmi &&
#sudo docker build -t gfr . --rm=true && sudo docker run -it gfr /bin/sh
# RUN ON AWS sudo docker run -p 80:3000 gfr -v ./app:/app -v ./restapi:/restapi &
#sudo docker images prune || sudo docker rmi $(sudo docker images -q)
#sudo dpkg-reconfigure docker
#sudo service docker restart

FROM mhart/alpine-node:8

# If you have native dependencies, you'll need extra tools
RUN apk add --no-cache make gcc g++ python
#RUN apk add aws-cli
#RUN yum install -y aws-cli

#https://github.com/ughly/alpine-aws-cli/blob/master/Dockerfile
RUN apk --no-cache update && \
    apk --no-cache add python py-pip py-setuptools ca-certificates curl groff less && \
    pip --no-cache-dir install awscli && \
    rm -rf /var/cache/apk/*

RUN npm install -g node-sass npx --unsafe-perm

#sudo du -hs /var/lib/docker/volumes
#(cd /var/lib/docker; sudo du -sh aufs  containers  image  network  plugins  swarm  tmp  trust  volumes)
#sudo rm -rf /var/lib/docker/aufs
#VOLUME /restapi/node_modules
#VOLUME /app/node_modules # causes vue mismatch
#VOLUME /restapi
#VOLUME /app

COPY app/package.json /app/package.json
RUN cd app && ls -la && npm install --production
RUN cd app && npm install node-sass --unsafe-perm # unsafe perms because node-sass

COPY restapi/package.json /restapi/package.json
RUN cd restapi && ls -la && npm install --production --unsafe-perm

#WORKDIR /app
COPY ./app /app
#WORKDIR /restapi
COPY ./restapi /restapi

RUN cd app && npm run prod

RUN rm -rf /tmp/* /var/tmp/* /var/cache/* /var/lib/apt/lists/* ~/.npm ~/.node-gyp ~/.node-gyp

WORKDIR /restapi
EXPOSE 3000

RUN npm install -g nodemon

CMD ["nodemon", "."]
# CMD ["node", "."]
