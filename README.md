Building
=

    cd app
    npm install
    npm run prod # populate ./restapi/client with frontend files

Running
=

Once built front-end - one can start the server

    set -e
    test -e testapi/client/*.js
    cd restapi
    npm install
    node .
    firefox http://127.1:3000 # open the dashboard