'use strict';

const driverNumbersControl = `-- 'DRIVER NUMBERS' and include total, treatment and control values (this shall look the same as the current USER NUMBERS section)
select count(distinct("user".id)) from "user"
left join usertrip on usertrip.userid = "user".id
where usertrip.userid in
            (
            select subscription.userid from subscription
            where subscription.subscriptiontypeid = 'SIRA_USER_CONTROL'
            )
and "user".email not like '%gofar.co'
and "user".email not like '%.pl';`

const driverNumbersTreatment = `select count(distinct("user".id)) from "user"
left join usertrip on usertrip.userid = "user".id
where usertrip.userid in
            (
            select subscription.userid from subscription
            where subscription.subscriptiontypeid = 'SIRA_USER_DEFAULT'
            )
and "user".email not like '%gofar.co'
and "user".email not like '%.pl';`

const dailyControl = `-- 'DAILY ACTIVE DRIVERS' and include total, treatment and control values

select count(distinct("user".id)) from "user"
left join usertrip on usertrip.userid = "user".id
left join trip on usertrip.tripid = trip.id
where usertrip.userid in
            (
            select subscription.userid from subscription
            where subscription.subscriptiontypeid = 'SIRA_USER_CONTROL'
            )
and trip.starttime between (current_date - interval '2 day') AND (current_date - interval '1 day')
and "user".email not like '%gofar.co'
and "user".email not like '%.pl';`

const dailyTreatment = `select count(distinct("user".id)) from "user"
left join usertrip on usertrip.userid = "user".id
left join trip on usertrip.tripid = trip.id
where usertrip.userid in
            (
            select subscription.userid from subscription
            where subscription.subscriptiontypeid = 'SIRA_USER_DEFAULT'
            )
and trip.starttime between (current_date - interval '2 day') AND (current_date - interval '1 day')
and "user".email not like '%gofar.co'
and "user".email not like '%.pl';`

const weeklyControl = `-- 'WEEKLY ACTIVE DRIVERS' and include total, treatment and control values

select count(distinct("user".id)) from "user"
left join usertrip on usertrip.userid = "user".id
left join trip on usertrip.tripid = trip.id
where usertrip.userid in
            (
            select subscription.userid from subscription
            where subscription.subscriptiontypeid = 'SIRA_USER_CONTROL'
            )
and trip.starttime between (current_date - interval '8 day') AND (current_date - interval '1 day')
and "user".email not like '%gofar.co'
and "user".email not like '%.pl';`

const weeklyTreatment = `select count(distinct("user".id)) from "user"
left join usertrip on usertrip.userid = "user".id
left join trip on usertrip.tripid = trip.id
where usertrip.userid in
            (
            select subscription.userid from subscription
            where subscription.subscriptiontypeid = 'SIRA_USER_DEFAULT'
            )
and trip.starttime between (current_date - interval '8 day') AND (current_date - interval '1 day')
and "user".email not like '%gofar.co'
and "user".email not like '%.pl';`

let sql;

module.exports = {
  getDailyControlDrivers: (dbh) => dbh.one(dailyControl),

  getDailyTreatmentDrivers: (dbh) => dbh.one(dailyTreatment),

  getWeeklyControlDrivers: (dbh) => dbh.one(weeklyControl),

  getWeeklyTreatmentDrivers: (dbh) => dbh.one(weeklyTreatment),

  async percentileTreatment(dbh, score) {
    const prom = await dbh.one(`select 1 - percent_rank(${score})
      WITHIN GROUP (ORDER BY leaderboard.score asc) as percent_rank
      from leaderboard where leaderboard.tripcount > 0;
    `)
    return Math.round(prom.percent_rank * 100)
  },
  async percentileControl(dbh, score) {
    const prom = await dbh.one(`select 1 - percent_rank(${score})
      WITHIN GROUP (ORDER BY leaderboard.score asc) as percent_rank
      from leaderboard where leaderboard.tripcount > 0;
    `)
    return Math.round(prom.percent_rank * 100)
  },
};
