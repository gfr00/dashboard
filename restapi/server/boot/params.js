'use strict';

const dbcfg = require('dotenv').config()['parsed'];

let initOptions2 = {
  // host: dbcfg['DB_HOST'] || process.env.DB_HOST,
  // username: dbcfg['DB_USER'] || process.env.DB_USER,
  // password: dbcfg['DB_PASS'] || process.env.DB_PASS,
  // database: dbcfg['DB_NAME'] || process.env.DB_NAME,
};

let pgp = require('pg-promise')(initOptions2);

let db = pgp(process.env.DB || 'postgresql://username:password@host:port/database');

let cache = {};
let time = 0;
const period =
  new Date(0, 0, 0, 0, 3, 0).getTime() - // years, months, days, hours, minutes, seconds
  new Date(0, 0, 0, 0, 0, 0).getTime();

const dbh = {
  end: () => db.end(),
  query: function(sql) {
    return Array.isArray(sql) ?
      db.tx(t => t.batch(sql.map(s => t.any(s)))) :
      db.any(sql);
  },
  one: function(sql) {
    return Array.isArray(sql) ?
      db.tx(t => t.batch(sql.map(s => t.one(s)))) :
      db.one(sql);
  },
};

function getOrdinal(n) {
  var s = ['th', 'st', 'nd', 'rd'];
  var v = n % 100;
  return (s[(v-20)%10]||s[v]||s[0]);
}

const dbUtil = require('../dbutil');

function fromDataBase(callback) {
  const thisTime = new Date().getTime();
  if (Math.abs(thisTime - time) < period) { // cache still active
    console.info('returning from cache', new Date());
    return callback(cache);
  }
  console.info('fetching new');
  time = thisTime;
  const distance = `select sum(trip.distance) from trip where trip.id in
    (
    select usertrip.tripid from usertrip where usertrip.userid in
      (
      select subscription.userid from subscription
      where subscription.subscriptiontypeid = $1
      )
    );`;
  const d = [
    dbh.one(distance.replace('\$1', "'SIRA_USER_CONTROL'")),
    dbh.one(distance.replace('\$1', "'SIRA_USER_DEFAULT'")),
    dbh.one(
      `select count(*) from "user" where "user".id in
        (
        select subscription.userid from subscription
        where subscription.subscriptiontypeid = 'SIRA_USER_CONTROL'
        )
      and "user".email not like '%gofar.co';` ||
        `select count (*) from lead
        where lead.plan = 'CONTROL' and lead.email not like '%gofar.co';`),
    dbh.one(
      `select count(*) from "user" where "user".id in
        (
        select subscription.userid from subscription
        where subscription.subscriptiontypeid = 'SIRA_USER_DEFAULT'
        )
      and "user".email not like '%gofar.co';` || `select count (*) from lead
      where lead.plan = 'DEFAULT' and lead.email not like '%gofar.co';`),
    dbh.one(`select
      avg(leaderboard."accelerationScore") as accScore,
      avg(leaderboard.brakingscore) as brkScore, avg(leaderboard.score) as score
      from leaderboard where leaderboard.userid in
        (
        select subscription.userid from subscription
        where subscription.subscriptiontypeid = 'SIRA_USER_CONTROL'
        )
        and leaderboard.tripcount > 0;`),
    dbh.one(`select
    avg(leaderboard."accelerationScore") as accScore,
    avg(leaderboard.brakingscore) as brkScore, avg(leaderboard.score) as score
    from leaderboard where leaderboard.userid in
      (
      select subscription.userid from subscription
      where subscription.subscriptiontypeid = 'SIRA_USER_DEFAULT'
      )
      and leaderboard.tripcount > 0;`),
  ];

  Promise.all(d).then(async d => {
    // console.log('d', d);
    const kmctrl = Math.round(d[0].sum * 1);
    const kmtr = Math.round(d[1].sum * 1);
    const u2 = Math.round(d[2].count * 1);
    const u1 = Math.round(d[3].count * 1);

    const scoresCtrl = d[4];
    const scoresTr = d[5];

    const dailyDriversCtrl = (await dbUtil.getDailyControlDrivers(dbh)).count * 1;
    const dailyDriversTr = (await dbUtil.getDailyTreatmentDrivers(dbh)).count * 1;

    const weeklyDriversCtrl = (await dbUtil.getWeeklyControlDrivers(dbh)).count * 1;
    const weeklyDriversTr = (await dbUtil.getWeeklyTreatmentDrivers(dbh)).count * 1;

    if (scoresCtrl && scoresTr) {
      const percTr = await dbUtil.percentileTreatment(dbh, scoresTr.score);
      const percCtrl = await dbUtil.percentileControl(dbh, scoresCtrl.score);

      let tmp1, tmp2, tmp3, tmp4;
      tmp1 = Math.round(scoresTr.accscore * 1);
      tmp2 = Math.round(scoresTr.brkscore * 1);
      tmp3 = Math.round(scoresCtrl.accscore * 1);
      tmp4 = Math.round(scoresCtrl.brkscore * 1);
      cache = {
        kmtotal: (kmtr + kmctrl).toLocaleString(),
        kmtr: kmtr.toLocaleString(),
        kmctrl: kmctrl.toLocaleString(),
        users: u1 + u2,
        userstr: u1,
        usersctrl: u2,

        // driver percentiles
        drppTr: [percTr, 100, getOrdinal(percTr) || '%'],
        drppCtrl: [ percCtrl, 100, getOrdinal(percCtrl) || '%'],

        driversAccelTr: [tmp1, 100],
        driversBrakingTr: [tmp2, 100],

        driversAccelCtrl: [tmp3, 100],
        driversBrakingCtrl: [tmp4, 100],

        driversTotalTr: [tmp1 + tmp2, 200],
        driversTotalCtrl: [tmp3 + tmp4, 200],

        dailyDrivers: dailyDriversCtrl + dailyDriversTr,
        dailyDriversCtrl,
        dailyDriversTr,

        weeklyDrivers: weeklyDriversCtrl + weeklyDriversTr,
        weeklyDriversCtrl,
        weeklyDriversTr,
      };
      return callback(cache);
    }
  });
}

module.exports = function(server) {
  var router = server.loopback.Router();
  // router.param('login', e => console.log(e));
  // router.param('password', e => console.log(e));
  router.get('/params', function(req, res, next) {
    // const has = req.params['login'] && req.params['password'];
    // const correct = req.params['login'] === 'SIRA_NSW' &&
    //   req.params['password'] === 'SIRAdata';
    // if (!correct) {
    //   return correct ? res.json({status: 'OK'}) : next('Wrong login data');
    // }
    const r = (range, min = 0) => Math.floor(Math.random() * range) + min;
    const kmtr = r(1e5);
    const kmctrl = r(1e4);
    const u1 = r(1e3, 100);
    const u2 = r(1e3, 200);
    let tmp0a, tmp0b, tmp1, tmp2, tmp3, tmp4;

    tmp0a = r(40, 10);
    tmp0b = r(50, 20);
    tmp1 = r(25);
    tmp2 = r(25);
    tmp3 = r(25);
    tmp4 = r(25);

    try {
      fromDataBase(d => res.json(d));
    } catch (error) {
      res.json({
      kmtotal: kmtr + kmctrl,
      kmtr,
      kmctrl,
      users: u1 + u2,
      userstr: u1,
      usersctrl: u2,

      // driver percentiles
      drppTr: [tmp0a, 100, 'th'],
      drppCtrl: [tmp0b, 100, 'nd'],

      driversAccelTr: [tmp1, 100],
      driversBrakingTr: [tmp2, 100],

      driversAccelCtrl: [tmp3, 100],
      driversBrakingCtrl: [tmp4, 100],

      driversTotalTr: [tmp1 + tmp2, 200],
      driversTotalCtrl: [tmp3 + tmp4, 200],
      });
    }
  });
  server.use(router);
};
